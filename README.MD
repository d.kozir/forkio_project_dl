The list of technologies that were used:
1. HTML
2. CSS
3. JavaScript
4. Gulp
5. NPM Modules:
gulp-sass
browser-sync
gulp-js-minify
gulp-uglify
gulp-clean-css
gulp-clean
gulp-concat
gulp-imagemin
gulp-autoprefixer

Project participants: Darya Kozir and Lisa Gidrevich

Tasks:
Darya Kozir:
- Layout of header
- Layout of section People Are Talking About Fork
- Configuring Gulp

Lisa Gidrevich:
- Layout of section Revolutionary Editor
- Layout of section Here is what you get
- Layout of section Fork Subscription Pricing